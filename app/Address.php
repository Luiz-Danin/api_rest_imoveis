<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'adresses';
    
    public function states()
    {
        return $this->belongsTo(State::class);
    }
    
    public function city()
    {
        return $this->belongsTo(Country::class);
    }
    
    public function real_state()
    {
        return $this->hasOne(RealState::class);
    }
}
