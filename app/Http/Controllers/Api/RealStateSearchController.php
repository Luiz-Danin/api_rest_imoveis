<?php

namespace App\Http\Controllers\Api;

use App\RealState;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\RealStateRepository;
use App\Api\ApiMessages;

class RealStateSearchController extends Controller
{
    
    private $realState;


    public function __construct(RealState $realState)
    {
        $this->realState = $realState;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //$realState = $this->realState->paginate(10);
        $repository = new RealStateRepository( $this->realState);
        
        if ($request->has('coditions')) {
            $repository->selectCoditions($request->get('coditions'));
        }

        if ($request->has('fields')) {
            $repository->selectFilter($request->get('fields'));
        }
        
        $repository->setLocation($request->all(['state', 'city']) );

        return response()->json(['data'=>$repository->getResult()->paginate(10) ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            
            $realState = $this->realState->with('address')->with('photos')->findOrFail($id);
            
            return response()->json(['data'=>$realState], 200);
            
        } catch (Exception $e){
            
            $message = new ApiMessages($e->getMessage());
            return response()->json($message->getMessage(), 401);
        }
        
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
