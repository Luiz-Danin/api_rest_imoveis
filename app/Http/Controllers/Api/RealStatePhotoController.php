<?php

namespace App\Http\Controllers\Api;

use App\Api\ApiMessages;
use App\RealStatePhoto;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class RealStatePhotoController extends Controller
{
    
    private $realStatePhoto;
    
    public function __construct(RealStatePhoto $realStatePhoto) {
        return $this->realStatePhoto = $realStatePhoto;
    }
    
    public function setThumb($photoId, $realStateId)
    {
        try{
            
            $photo = $this->realStatePhoto
                    ->where('real_state_id',$realStateId)
                    ->where('is_thumb', true);
            //dd($photo);
            if ( $photo->count() ){
                $photo->first()->update(['is_thumb' => false]);
            
            $new_photo = $this->realStatePhoto->find($photoId);
            $new_photo->update(['is_thumb'=>true]);
            
            return response()->json(
                [
                'data'=>[
                        'mensagem'=>'Thumb atualizada com sucesso'
                    ]
                ],
                    200);
            }
            
            return response()->json(
                [
                    'mensagem'=>'Thumb não encontrada'
                ],
                    200);
        }
        catch (Exception $e){
            $message = new ApiMessages( $ex->getMessage() );
            return response()->json($message->getMessage(), 401 );
        }
    }
    
    public function remove($photoId)
    {
        try{
            
            $photo = $this->realStatePhoto->find($photoId);
            
            if ( isset($photo->is_thumb) ) {
                $message = new ApiMessages('Não é possivel remover foto de thumb, selecione outra thumb e remova a imagem desejada!');

                return response()->json($message->getMessage(), 401);
            }

            if ($photo) {
                
                Storage::disk('public')->delete($photo->photo);
                $photo->delete();
                
                return response()->json(
                    [
                    'data'=>[
                            'mensagem'=>'Thumb atualizada com sucesso'
                        ]
                    ],
                        200);
            }
            
            return response()->json(
                [
                    'mensagem'=>'Thumb não encontrada'
                ],
                    200);
        }
        catch (Exception $e){
            $message = new ApiMessages( $ex->getMessage() );
            return response()->json($message->getMessage(), 401 );
        }
    }
}
