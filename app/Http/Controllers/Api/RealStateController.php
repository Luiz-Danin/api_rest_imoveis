<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\RealStateRequest;
use App\RealState;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Api\ApiMessages;

class RealStateController extends Controller
{
    private $realState;


    public function __construct(RealState $real_state)
    {
        $this->realState = $real_state;
    }
    
    public function index()
    {
        $realStates = auth('api')->user()->real_state();
        //$realStates = $this->realState->with('photos')->paginate(10);
        
        return response()->json($realStates->paginate(10), 200);
    }
    
    public function show($id)
    {
        try {
            
            //$realState = $this->realState->with('photos')->findOrFail($id);
            $realState = auth('api')->user()->real_state()
                                            ->with('photos')
                                            ->findOrFail($id)->makeHidden('thumb');
            
            return response()->json(
                [
                'data'=>[$realState]
                ],
                    200);
            
        } catch (Exception $ex) {
            
            $message = new ApiMessages( $ex->getMessage() );
            return response()->json($message->getMessage(), 401 );
            //return response()->json(['error'=>$ex->getMessage()], 401 );
        }
    }

    public function store(RealStateRequest $request)
    {
        $data = $request->all();
        
		$images = $request->file('images');

    	try{
                    $data['user_id'] = auth('api')->user()->id;

		    $realState = $this->realState->create($data);

    		if(isset($data['categories']) && count($data['categories'])) {
    			$realState->categories()->sync($data['categories']);
		    }

		    if($images) {
    			$imagesUploaded = [];

    			foreach ($images as $image) {
    				$path = $image->store('images', 'public');
    				$imagesUploaded[] = ['photo' => $path, 'is_thumb' => false];
			    }
                                //dd($imagesUploaded);

			    $realState->photos()->createMany($imagesUploaded);
		    }

    		return response()->json([
    			'data' => [
    				'msg' => 'Imóvel cadastrado com sucesso!'
			    ]
		    ], 200);

	    } catch (\Exception $e) {
		    $message = new ApiMessages($e->getMessage());
		    return response()->json($message->getMessage(), 401);
	    }
    }
    
    public function update($id, RealStateRequest $request)
    {
        $data = $request->all();
        $images = $request->file('images');
        
        try {
            
            //$realState = $this->realState->findOrFail($id);
            $realState = auth('api')->user()->real_state()->findOrFail($id);
            $realState->update($data);
            
            if(isset($data['categories']) && count($data['categories'])>0 ) {
                $realState->categories()->sync($data['categories']);
            }
            
            if($images) {
    			$imagesUploaded = [];

    			foreach ($images as $image) {
    				$path = $image->store('images', 'public');
				$imagesUploaded[] = ['photo' => $path, 'is_thumb' => false];
			    }

			    $realState = $realState->photos()->createMany($imagesUploaded);
		    }
                    //dd($realState);
            return response()->json(
                [
                'data'=>[
                        'mensagem'=>'Imóvel atualizado com sucesso'
                    ]
                ],
                    200);
        } catch (Exception $ex) {
            
            $message = new ApiMessages( $ex->getMessage() );
            return response()->json($message->getMessage(), 401 );
            //return response()->json(['error'=>$ex->getMessage()], 401 );
        }
    }
    
    public function destroy($id)
    {
        try {
            
            //$realState = $this->realState->findOrFail($id);
            $realState = auth('api')->user()->real_state()->findOrFail($id);
            $realState->delete();
            
            return response()->json(
                [
                'data'=>[
                        'mensagem'=>'Imóvel removido com sucesso'
                    ]
                ],
                    200);
        } catch (Exception $ex) {
            $message = new ApiMessages( $ex->getMessage() );
            return response()->json($message->getMessage(), 401 );
            //return response()->json(['error'=>$ex->getMessage()], 401 );
        }
    }
}
