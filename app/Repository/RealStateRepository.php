<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repository;

/**
 * Description of RealStateRepository
 *
 * @author luiz
 */

class RealStateRepository extends AbstractRepository {
    
    private $location;
    
    public function setLocation(array $data): self
    {
        $this->location = $data;
        return $this;
    }

    public function getResult()
    {
        //parent::getResult();
        $location  = $this->location;
        
        return $this->model->whereHas('address', function($q) use($location){
            
            $q->where('state_id', $location['state'])
              ->where('city_id',$location['city']);  
            //add condition with where clousure
        });
    }
}
